---
title: About Conner
subtitle: Why you'd want to work with me
comments: true
---

My name is Conner. I have the following qualities:

- I rock a great beard
- I'm extremely loyal to my friends
- I like woodworking
- Love technology and coding

That rug really tied the room together.

### my history

To be honest, I'm having some trouble remembering right now, so why don't you
just watch [my movie](https://en.wikipedia.org/wiki/The_Big_Lebowski) and it
will answer **all** your questions.
